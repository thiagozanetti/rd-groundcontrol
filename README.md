# RD Ground Control

This is ground control to Major Tom :rocket:

## How to use

First you need to install the dependencies:

```sh
npm install
```

then type:

```sh
npm start
```

A new browser window will open at http://localhost:5000