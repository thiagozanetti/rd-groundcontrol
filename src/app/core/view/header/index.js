import m from 'mithril'

import './header.css'

export default m('header.header', [
  m('h3.title', '🚀 Ground control to Major Tom!'),
])
