import m from 'mithril'

const baseUrl = 'http://localhost:8080/api/v1/'

const baseService = (method, resource) => (data = {}, urlParams = [], queryParams = [], headers = {}) => {
  const reducedUrlParams = urlParams.reduce((acc, item) => acc = acc.concat(`${item}/`), '/')
  const reducedQueryParams = queryParams.reduce((acc, item) => acc = acc.concat(`${item.key}=${item.value}`), '')

  const url = `${baseUrl}${resource}${reducedUrlParams}${!!reducedQueryParams ? `?${reducedQueryParams}` : ''}`

  headers = Object.assign(headers,  { 'content-type': 'application/json' })

  return m.request({
    method,
    url,
    data,
    headers,
  })
}

export default baseService