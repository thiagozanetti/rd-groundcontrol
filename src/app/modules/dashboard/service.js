import baseService from '../../core/service'

export const getVisits = baseService('GET', 'visits')
export const getContacts = baseService('GET', 'contacts')

export default {
  getVisits,
  getContacts,
}