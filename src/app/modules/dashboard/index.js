import m from 'mithril'

import Visits from '../visits'
import { getVisits, getContacts } from './service'

import './dashboard.css'

let visits = []

export default {
  oncreate(vnode) {    
    Promise.all([getContacts(), getVisits()]).then((values) => {
      visits = values[0].map((contact) => {
        const entries = values[1].filter(value => value.uid === contact.uid)  
        return Object.assign(contact, { entries })
      })
    })
  },
  view() {
    return m('.dashboard', [
      m('h3.sub-title', 'Those were the visitors:'),
      m(Visits, { visits })
    ])
  }
}