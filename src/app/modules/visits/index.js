import m from 'mithril'

import Visit from '../visit'

import './visits.css'

export default {
  view(vnode) {
    const visits = vnode.attrs.visits
    
    return m('.visits', visits.map(visit => m(Visit, { visit })))
  }
}