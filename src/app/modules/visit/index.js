import m from 'mithril'
import moment from 'moment'

import './visit.css'

export default {
  view(vnode) {
    const visit = vnode.attrs.visit
    
    return m('.visit', [
      m('.visit-header',[
        m('span.name', visit.name || 'Not Identified'),
        m('span.email', visit.email || 'n/a'),
      ]),
      m('span.title', 'Those were the location visited:'),
      m('ul', visit.entries.map(entry => m('li', `${moment(entry.when).format('DD/MM/YYYY HH:mm:ss')} - ${entry.to}`)))
    ])
  }
}